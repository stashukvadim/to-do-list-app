To create a table 'tasks' in test schema, run query from src\main\resources\createTable.sql

Specify properties for connection to database in src\main\resources\hibernate.cfg.xml

By default .war file is saved in 'target' directory, but you can change directory in pom.xml in 'tomcat.app.dir' property.
Alternatively you can change directory with command: mvn clean install -Dtomcat.app.dir={path.to.directory}

See example screenshots of the app in a 'screenshots' folder
