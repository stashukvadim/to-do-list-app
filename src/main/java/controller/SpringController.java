package controller;

import domain.Task;
import domain.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;


@Controller
@RequestMapping("/")
public class SpringController {

    @Inject
    private TaskService taskService;

    @RequestMapping(method = RequestMethod.GET)
    public String startPage(ModelMap model) {
        model.addAttribute("tasks", taskService.getAllTasks());
        return "start";
    }

    @RequestMapping(value = "/markTaskAsDone", method = RequestMethod.POST)
    public String markTaskAsDone(@RequestParam(value = "id") String id) {
        taskService.markAsDone(new Task(id, "", null));
        return "redirect:/";
    }

    @RequestMapping(value = "/markTaskAsActive", method = RequestMethod.POST)
    public String markTaskAsActive(@RequestParam(value = "id") String id) {
        taskService.markAsActive(new Task(id, "", null));
        return "redirect:/";
    }

    @RequestMapping(value = "/deleteTask", method = RequestMethod.POST)
    public String deleteTask(@RequestParam(value = "id") String id) {
        taskService.deleteTask(new Task(id, "", null));
        return "redirect:/";
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.GET)
    public String openAddTask() {
        return "addTask";
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public String addTask(@RequestParam(value = "description") String description) {
        taskService.addTask(new Task(description));
        return "redirect:/";
    }

    @RequestMapping(value = "/editTask", method = RequestMethod.POST)
    public String editDescription(@RequestParam(value = "id") String id,
                                  @RequestParam(value = "description") String description,
                                  ModelMap model) {
        model.addAttribute("id", id);
        model.addAttribute("description", description);
        return "editTask";
    }

    @RequestMapping(value = "/editTaskWithDescription", method = RequestMethod.POST)
    public String editDescription(@RequestParam(value = "id") String id,
                                  @RequestParam(value = "description") String description) {
        taskService.editDescription(new Task(id, description, null));
        return "redirect:/";
    }
}
