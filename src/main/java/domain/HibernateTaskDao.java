package domain;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;

@Repository
public class HibernateTaskDao implements TaskDao {

    SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    @Override
    public void saveTask(Task task) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(task);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateDescription(Task task) {
        Task taskFromDb = getTask(task);
        taskFromDb.setTaskDescription(task.getTaskDescription());
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(taskFromDb);
            session.getTransaction().commit();
        }
    }

    @Override
    public void markAsDone(Task task) {
        Task taskFromDb = getTask(task);
        taskFromDb.setStatus(Task.Status.DONE);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(taskFromDb);
            session.getTransaction().commit();
        }
    }

    @Override
    public void markAsActive(Task task) {
        Task taskFromDb = getTask(task);
        taskFromDb.setStatus(Task.Status.ACTIVE);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(taskFromDb);
            session.getTransaction().commit();
        }
    }

    @Override
    public void removeTask(Task task) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(task);
            session.getTransaction().commit();
        }
    }

    @Override
    public Task getTask(Task task) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Task taskFromDb = session.get(Task.class, task.getId());
            session.getTransaction().commit();
            return taskFromDb;
        }
    }

    @Override
    public List<Task> findAll() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            return session.createCriteria(Task.class).addOrder(Order.desc("timeStamp")).list();
        }
    }

    @PreDestroy
    private void closeSessionFactory() {
        sessionFactory.close();
    }
}
