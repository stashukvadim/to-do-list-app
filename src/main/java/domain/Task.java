package domain;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="tasks")
public class Task {

    public enum Status{
        ACTIVE, DONE
    }

    @Column(name="description")
    private String taskDescription;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Id
    @Column(name = "id")
    private final String id;

    @Column(name="timestamp")
    private final Date timeStamp;

    protected Task() {
        this("");
    }

    public Task(String taskDescription) {
        this(UUID.randomUUID().toString(), taskDescription, Status.ACTIVE);
    }

    public Task(String id, String taskDescription, Status status) {
        this.id = id;
        this.status = status;
        this.taskDescription = taskDescription;
        timeStamp = new Date();
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public Status getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    void setStatus(Status status) {
        this.status = status;
    }

    void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        return !(id != null ? !id.equals(task.id) : task.id != null);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Task(" + taskDescription + ")";
    }
}
