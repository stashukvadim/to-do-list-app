package domain;

import java.util.List;

interface TaskDao {
    void saveTask(Task task);

    void updateDescription(Task task);

    void markAsDone(Task task);

    void markAsActive(Task task);

    void removeTask(Task task);

    Task getTask(Task task);

    List<Task> findAll();
}
