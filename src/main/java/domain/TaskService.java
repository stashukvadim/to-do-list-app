package domain;

import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class TaskService {

    @Inject
    private TaskDao dao;

    public void addTask(Task task) {
        dao.saveTask(task);
    }

    public void editDescription(Task task) {
        dao.updateDescription(task);
    }

    public void markAsDone(Task task) {
        dao.markAsDone(task);
    }

    public void markAsActive(Task task) {
        dao.markAsActive(task);
    }

    public void deleteTask(Task task) {
        dao.removeTask(task);
    }

    public List<Task> getAllTasks() {
        return dao.findAll();
    }
}
