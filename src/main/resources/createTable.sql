CREATE TABLE `test`.`tasks` (
  `id` VARCHAR(60) NOT NULL COMMENT '',
  `description` VARCHAR(1000) NOT NULL COMMENT '',
  `status` ENUM('ACTIVE', 'DONE') NOT NULL COMMENT '',
  `timestamp` DATETIME NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)  COMMENT '')
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_general_ci;