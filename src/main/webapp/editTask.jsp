<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit task</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<form role="form" id="formId" method="post" action="${pageContext.request.contextPath}/editTaskWithDescription">
    <textarea id="textareaId" autofocus class="form-control" form="formId" name="description">${description}</textarea>
    <input type="hidden" value="${id}" name="id"/>
    <br>
    <input type="submit" class="btn btn-info" value="Edit task" align="center">
</form>
<script>
    $('#textareaId').keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 10 || e.keyCode == 13) {
            this.form.submit();
        }
    });
</script>
</body>
</html>
