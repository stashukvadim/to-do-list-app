<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>TODO-List</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script>function showActive() {
        $('tr').show();
        $('.status-done').hide();
    }

    function showDone() {
        $('tr').show();
        $('.status-active').hide();
    }

    function showAll() {
        $('tr').show();
    }
    </script>
    <style>
        .table tbody > tr > td {
            vertical-align: middle;
        }
        .panel-heading {
            font-size: 30px;
            text-align: center;
        }

        .fixed-size{
            width: 65px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">To Do</div>
        <table class="table table-hover">
            <th><a class="btn btn-primary" href="${pageContext.request.contextPath}/addTask">Add new task</a>
                <button class="btn btn-info" onclick="showAll()">Show All</button>
                <button class="btn btn-info" onclick="showActive()">Show Active</button>
                <button class="btn btn-info" onclick="showDone()">Show Done</button>
            </th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <c:forEach items="${tasks}" var="task">
                <c:set var="isStatusActive" value="true"/>
                <c:set var="status" value="status-active"/>
                <c:if test="${task.status=='DONE'}">
                    <c:set var="isStatusActive" value="false"/>
                    <c:set var="status" value="status-done"/>
                </c:if>

                <tr class="${status}">
                    <td class="col-md-8">
                        <c:if test="${!isStatusActive}">
                        <del>
                            </c:if>
                            <c:out value="${task.taskDescription}"/>
                            <c:if test="${!isStatusActive}">
                        </del>
                        </c:if></td>
                    <td></td>
                    <td>
                        <c:if test="${isStatusActive}">
                            <form class="form" method="post" action="${pageContext.request.contextPath}/markTaskAsDone">
                                <input type="hidden" value="${task.id}" name="id"/>
                                <button style="width: 65px" type="submit" class='btn btn-success btn-sm fixed-size'>
                                    Done
                                </button>
                            </form>
                        </c:if>

                        <c:if test="${!isStatusActive}">
                        <form class="form" method="post" action="${pageContext.request.contextPath}/markTaskAsActive">
                            <input type="hidden" value="${task.id}" name="id"/>
                            <button class="btn btn-warning btn-sm fixed-size">
                                Activate
                            </button>
                            </c:if>

                        </form>
                    </td>
                    <td>
                        <form class="form" method="post" action="${pageContext.request.contextPath}/editTask">
                            <input type="hidden" value="${task.id}" name="id"/>
                            <input type="hidden" value=${task.taskDescription} name="description"/>
                            <button type="submit" class="btn btn-primary btn-sm fixed-size">
                                Edit
                            </button>
                        </form>
                    </td>
                    <td>
                        <form class="form" method="post" action="${pageContext.request.contextPath}/deleteTask">
                            <input type="hidden" value="${task.id}" name="id"/>
                            <button type="submit" class="btn btn-danger btn-sm fixed-size">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
